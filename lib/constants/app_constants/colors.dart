import 'package:flutter/material.dart';

const kPrimaryColor = Colors.pink;
const kAccentColor = Colors.pinkAccent;
const kScreenBackgroundColor = Color(0xFFE0F7FA);

const kTextColor = Colors.black;
const kTextDangerColor = Color(0xFFDC3027);
const kTextSuccessColor = Color(0xFF00C851);
const kTextInfoColor = Colors.blueAccent;
const kTextNoInternetColor = Colors.red;

const kAppBarBackgroundColor = Colors.pink;
const kAppBarTextColor = Colors.white;
