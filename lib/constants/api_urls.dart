class ApiUrls {
  static final String baseUrl = 'https://reqres.in/api/';
  static final String login = 'login';
  static final String list = 'users?page=2';
}
