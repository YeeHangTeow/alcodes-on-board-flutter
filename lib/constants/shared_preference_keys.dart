class SharedPreferenceKeys {
  static final String userEmail = 'user_email';
  static final String userToken = 'user_token';
}
