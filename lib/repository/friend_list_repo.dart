import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:dio/dio.dart';

class FriendListRepo {
  Future<List<FriendListResponseModel>> friendListAsync() async {
    try {
      final dio = Dio();
      dio.options.baseUrl = ApiUrls.baseUrl;

      final response = await dio.get(ApiUrls.list);

      List<FriendListResponseModel> mData = [];
      Map<String, dynamic> myMap = new Map<String, dynamic>.from(response.data);
      for (int i = 0; i < response.data.length; i++) {
        FriendListResponseModel model = new FriendListResponseModel(
            id: myMap['data'][i]["id"],
            firstName: myMap['data'][i]["first_name"],
            lastName: myMap['data'][i]["last_name"],
            avatar: myMap['data'][i]["avatar"],
            email: myMap['data'][i]["email"]);
        mData.add(model);
      }
      return mData;
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }
}
