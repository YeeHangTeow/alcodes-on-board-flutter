import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      // Check if user is logged in.
      final sharedPref = await SharedPreferences.getInstance();

      // Short form:
      // final isLoggedIn = (sharedPref.getString(SharedPreferenceKeys.userEmail)?.isNotEmpty ?? false);

      // Or easy read:
      final userEmail = sharedPref.getString(SharedPreferenceKeys.userEmail);
      final isLoggedIn = (userEmail != null && userEmail.isNotEmpty);

      String routeName;

      if (isLoggedIn) {
        routeName = AppRouter.home;
      } else {
        routeName = AppRouter.login;
      }

      Navigator.of(context)
          .pushNamedAndRemoveUntil(routeName, (route) => false);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(),
    );
  }
}
