import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:flutter/material.dart';

class MyFriendDetails extends StatefulWidget {
  FriendListResponseModel arguments;

  MyFriendDetails({this.arguments});

  @override
  _MyFriendDetailsState createState() => _MyFriendDetailsState();
}

class _MyFriendDetailsState extends State<MyFriendDetails> {
  FriendListResponseModel arguments;

  _MyFriendDetailsState({this.arguments});

  @override
  Widget build(BuildContext context) {
    print(widget.arguments.id);
    return Scaffold(
        appBar: AppBar(
          title: Text("Friend Details"),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(30, 40, 30, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: CircleAvatar(
                      backgroundImage: NetworkImage(widget.arguments.avatar),
                      radius: 70),
                ),
                SizedBox(height: 50),
                Text('First Name : ',
                    style: TextStyle(
                      fontSize: 18,
                    )),
                SizedBox(height: 10),
                Text(widget.arguments.firstName,
                    style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold)),
                SizedBox(height: 30),
                Text('Last Name : ',
                    style: TextStyle(
                      fontSize: 18,
                    )),
                SizedBox(height: 10),
                Text(widget.arguments.lastName,
                    style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold)),
                SizedBox(height: 30),
                Text('Email : ',
                    style: TextStyle(
                      fontSize: 18,
                    )),
                SizedBox(height: 10),
                Text(widget.arguments.email, style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold)),
                SizedBox(height: 30),
              ],
            )));
  }
}
