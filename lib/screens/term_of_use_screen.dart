import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermOfUse extends StatefulWidget {
  @override
  _TermOfUseState createState() => _TermOfUseState();
}

class _TermOfUseState extends State<TermOfUse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Term of Use'),
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: WebView(
            initialUrl:
                "https://generator.lorem-ipsum.info/terms-and-conditions",
            javascriptMode: JavascriptMode.unrestricted,
          ),
        ));
  }
}
