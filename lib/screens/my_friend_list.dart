import 'dart:async';

import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:alcodes_on_board_flutter/repository/friend_list_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

/// TODO Issue on this screen:
/// - Show list of my friends.
/// - Click list item go to my friend detail page.

class MyFriendList extends StatefulWidget {
  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {
  String _load = "loading";
  List<FriendListResponseModel> _friendList = [];

  Future<void> getData() async {
    var alertDialogStatus = AppAlertDialogStatus.error;
    var alertDialogMessage = '';

    try {
      final repo = FriendListRepo();
      repo.friendListAsync().then((mData) {
        setState(() {
          _load = "My Friend List";
          _friendList = mData;
        });
      });
    } catch (ex) {
      //return error statement
      Fimber.e('d;;Error Request getting list', ex: ex);
      alertDialogMessage = '$ex';
    }
    if (alertDialogMessage.isNotEmpty) {
      final appAlertDialog = AppAlertDialog();
      appAlertDialog.showAsync(
          context: context,
          status: alertDialogStatus,
          message: alertDialogMessage);
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$_load'),
      ),
      body: ListView.builder(
        itemCount: _friendList == null ? 0 : _friendList.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(10),
            child: Card(
              color: Color(0xFFB2EBF2),
              margin: const EdgeInsets.fromLTRB(3, 7, 3, 7),
              child: ListTile(
                onTap: () {
                  Navigator.of(context).pushNamed(AppRouter.myFriendDetail,
                      arguments: _friendList[index]);
                },
                title: Padding(
                  padding: const EdgeInsets.all(appConst.kDefaultPadding),
                  child: Text(
                      'Name: ${_friendList[index].firstName} ${_friendList[index].lastName}\nEmail: ${_friendList[index].email}',
                      style: TextStyle(
                        fontSize: 15,
                      )),
                ),
                leading: CircleAvatar(
                  radius: 50.0,
                  backgroundImage: NetworkImage(_friendList[index].avatar),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
