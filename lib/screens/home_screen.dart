import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen:
/// - Show confirm logout dialog before logout user.//complete
/// - Go to my friend list screen when click My Friend List button.//complete

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<_DataModel> _futureBuilder;
  _DataModel _dataModel;

  Future<void> _onLogoutButtonPressedAsync() async {
    // Clear data and go to login screen.
    final sharedPref = await SharedPreferences.getInstance();
    showDialog(
      context: context,
      child: AlertDialog(
        title: Text('Logout'),
        content: Text('Are you sure to sign out your account?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text(
              'No',
              style: TextStyle(color: Colors.lightBlue),
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(AppRouter.login, (route) => false);
            },
            child: Text(
              'Yes',
              style: TextStyle(
                color: Colors.red[700],
              ),
            ),
          ),
        ],
      ),

    );
    await sharedPref.clear();
    //Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.login, (route) => false);
  }

  void _onMyFriendListButtonPressed() {
    // Which type of navigator to use?

    Navigator.of(context).pushNamed(AppRouter.myFriendList);
    //Navigator.of(context).pushReplacementNamed(AppRouter.myFriendList);
    //Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.myFriendList, (route) => false);


  }

  Future<_DataModel> _getScreenDataAsync() async {
    final sharedPref = await SharedPreferences.getInstance();

    _dataModel = _DataModel();
    _dataModel.userEmail = sharedPref.getString(SharedPreferenceKeys.userEmail);

    return _dataModel;
  }

  @override
  void initState() {
    _futureBuilder = _getScreenDataAsync();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: AppBar(
        title: Text('Home'),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: _onLogoutButtonPressedAsync,
          ),
        ],
      ),
      body: FutureBuilder<_DataModel>(
        initialData: null,
        future: _futureBuilder,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            return _content();
          }
          return Center(
            child: Text('Error'),
          );
        },
      ),
    );
  }

  Widget _content() {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(appConst.kDefaultPadding),
          child: Text('Hello ${_dataModel.userEmail}!'),
        ),
        Expanded(
          child: Center(
            child: ElevatedButton(
              child: Text('My Friend List'),
              onPressed: _onMyFriendListButtonPressed,
            ),
          ),
        ),
      ],
    );
  }
}

class _DataModel {
  String userEmail;
}
