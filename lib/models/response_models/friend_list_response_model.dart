class FriendListResponseModel {
  int id;
  String firstName;
  String lastName;
  String avatar;
  String email;

  FriendListResponseModel(
      {this.id, this.firstName, this.lastName, this.avatar, this.email});
}
